<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Book
 * @author     nganly <lythanhngan167@gmail.com>
 * @copyright  2019 nganly
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Controller\BaseController;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Book', JPATH_COMPONENT);
JLoader::register('BookController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = BaseController::getInstance('Book');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
