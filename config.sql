-- ten cua hang
UPDATE `prfwj_eshop_configs` SET `config_value` = 'Camilla' WHERE `config_key` = 'store_name';

-- ten chu cua hang
UPDATE `prfwj_eshop_configs` SET `config_value` = 'Huỳnh Minh Tuyến' WHERE `config_key` = 'store_owner';

-- email cua hang
UPDATE `prfwj_eshop_configs` SET `config_value` = 'landadep@camillacosmetics.co' WHERE `config_key` = 'email';

-- dien thoai cua hang
UPDATE `prfwj_eshop_configs` SET `config_value` = '02871008100' WHERE `config_key` = 'telephone';

-- tien to ma don hang
UPDATE `prfwj_eshop_configs` SET `config_value` = 'CML' WHERE `config_key` = 'invoice_prefix';

-- dia chi cua hang
UPDATE `prfwj_eshop_configs` SET `config_value` = 'Level 46 & 56, 2 Hai Trieu Str., Ben Nghe Ward, Dist. 1, Ho Chi Minh City, Vietnam' WHERE `config_key` = 'address';


-- cap nhat Giao hang
UPDATE `prfwj_eshop_shippings` SET `title` = 'Camilla vận chuyển' WHERE `prfwj_eshop_shippings`.`name` = 'eshop_free';

-- cap nhat Thanh toan
-- phai vao admin: phan Thanh toan de cau hinh

-- cap nhat Don vi
-- phai vao admin: phan Don vi de cau hinh

-- cap nhat Tuy chon
-- phai vao admin: phan Tuy chon de cau hinh, xoa bot




-- UPDATE prfwj_content
-- SET `fulltext` = REPLACE(`fulltext`, 'Che3Mien', 'アジア Food');
-- UPDATE prfwj_content
-- SET introtext = REPLACE(introtext, 'Che3Mien', 'アジア Food');
--
-- UPDATE prfwj_content
-- SET `fulltext` = REPLACE(`fulltext`, 'Tươi Kìa', 'アジア Food');
-- UPDATE prfwj_content
-- SET introtext = REPLACE(introtext, 'Tươi Kìa', 'アジア Food');
--
--
-- UPDATE prfwj_content
-- SET `fulltext` = REPLACE(`fulltext`, 'tuoikia.com', 'chovietnam24h.com');
-- UPDATE prfwj_content
-- SET introtext = REPLACE(introtext, 'tuoikia.com', 'chovietnam24h.com');
--
-- UPDATE prfwj_content
-- SET `fulltext` = REPLACE(`fulltext`, 'Nhật Nam', 'アジア Food');
-- UPDATE prfwj_content
-- SET introtext = REPLACE(introtext, 'Nhật Nam', 'アジア Food');
--
--
-- UPDATE prfwj_eshop_productdetails
-- SET product_short_desc = REPLACE(product_short_desc,"Nhật Nam","アジア Food");
-- UPDATE prfwj_eshop_productdetails
-- SET product_desc = REPLACE(product_desc,"Nhật Nam","アジア Food");

-- UPDATE prfwj_eshop_products
-- SET product_sku = concat("",id);

UPDATE prfwj_modules
SET content = REPLACE(content, 'Tươi Kìa', 'Camilla')
WHERE module = 'mod_custom';


UPDATE prfwj_modules
SET content = REPLACE(content, 'Sạp 38 Chợ đầu mối Bình Điền, Quận 7', 'Level 46 & 56, 2 Hai Trieu Str., Ben Nghe Ward, Dist. 1, Ho Chi Minh City, Vietnam')
WHERE module = 'mod_custom';

UPDATE prfwj_modules
SET content = REPLACE(content, '0901.329.921', '02871008100')
WHERE module = 'mod_custom';

UPDATE prfwj_modules
SET content = REPLACE(content, '0901329921', '02871008100')
WHERE module = 'mod_custom';


UPDATE prfwj_modules
SET content = REPLACE(content, 'tuoikia@gmail.com', 'landadep@camillacosmetics.co')
WHERE module = 'mod_custom';


UPDATE `prfwj_contact_details`
SET `address` = 'Số 111 Trương Đình Hội, Phường 1, Quận 8, TP. Hồ Chí Minh',
`telephone` = '02871008100',
`mobile` = '02871008100',
`email_to` = 'landadep@camillacosmetics.co'
WHERE `prfwj_contact_details`.`id` = 2;


UPDATE prfwj_contact_details
SET misc = REPLACE(misc, 'Số 1 Trương Đình Hội, Phường 1, Quận 8, TP. Hồ Chí Minh', 'Level 46 & 56, 2 Hai Trieu Str., Ben Nghe Ward, Dist. 1, Ho Chi Minh City, Vietnam')
WHERE `prfwj_contact_details`.`id` = 2;

UPDATE prfwj_contact_details
SET misc = REPLACE(misc, '0901.329.921', '02871008100')
WHERE `prfwj_contact_details`.`id` = 2;

UPDATE prfwj_contact_details
SET misc = REPLACE(misc, '0901329921', '02871008100')
WHERE `prfwj_contact_details`.`id` = 2;

UPDATE prfwj_contact_details
SET misc = REPLACE(misc, 'Tươi Kìa', 'Camilla')
WHERE `prfwj_contact_details`.`id` = 2;



DELETE FROM `ipanigroup_db`.`prfwj_eshop_weightdetails`
WHERE `prfwj_eshop_weightdetails`.`id` = 3;

TRUNCATE prfwj_eshop_stock_product;
TRUNCATE prfwj_eshop_stock_user;
