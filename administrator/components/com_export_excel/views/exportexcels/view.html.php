<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Export_excel
 * @author     nganly <lythanhngan167@gmail.com>
 * @copyright  2020 nganly
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

use \Joomla\CMS\Language\Text;

/**
 * View class for a list of Export_excel.
 *
 * @since  1.6
 */
class Export_excelViewExportexcels extends \Joomla\CMS\MVC\View\HtmlView
{
	protected $items;

	protected $pagination;

	protected $state;

	protected $form;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');

		$mainframe =JFactory::getApplication();
		//$mainframe->setUserState( ".state_variable", "state1" );
		$month_default =  $this->state->get('filter.month', '');
		$year_default =  $this->state->get('filter.year', '');

		if($month_default == ''){
			$this->state->set('filter.month', date('m'));
		}
		if($year_default == ''){
			$this->state->set('filter.year', date('Y'));
		}
		//print_r($this->state);
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

    $this->filterForm = $this->get('FilterForm');
    $this->activeFilters = $this->get('ActiveFilters');
		$this->form = $this->get('Form');
		jimport('phpexcel.library.PHPExcel');
		$objPHPExcel = new PHPExcel();
				//print_r($objPHPExcel);
		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		Export_excelHelper::addSubmenu('exportexcels');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = Export_excelHelper::getActions();

		JToolBarHelper::title(Text::_('COM_EXPORT_EXCEL_TITLE_EXPORTEXCELS'), 'exportexcels.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/exportexcel';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('exportexcel.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('exportexcels.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('exportexcel.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('exportexcels.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('exportexcels.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'exportexcels.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('exportexcels.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('exportexcels.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'exportexcels.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('exportexcels.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_export_excel');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_export_excel&view=exportexcels');
	}

	/**
	 * Method to order fields
	 *
	 * @return void
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			'a.`state`' => JText::_('JSTATUS'),
			'a.`created_by`' => JText::_('COM_EXPORT_EXCEL_EXPORTEXCELS_CREATED_BY'),
			'a.`name`' => JText::_('COM_EXPORT_EXCEL_EXPORTEXCELS_NAME'),
			'a.`id_number`' => JText::_('COM_EXPORT_EXCEL_EXPORTEXCELS_ID_NUMBER'),
			'a.`phone`' => JText::_('COM_EXPORT_EXCEL_EXPORTEXCELS_PHONE'),
			'a.`level`' => JText::_('COM_EXPORT_EXCEL_EXPORTEXCELS_LEVEL'),
			'a.`total_money`' => JText::_('COM_EXPORT_EXCEL_EXPORTEXCELS_TOTAL_MONEY'),
			'a.`month`' => JText::_('COM_EXPORT_EXCEL_EXPORTEXCELS_MONTH'),
			'a.`id_parent`' => JText::_('COM_EXPORT_EXCEL_EXPORTEXCELS_ID_PARENT'),
			'a.`bank_name`' => JText::_('COM_EXPORT_EXCEL_EXPORTEXCELS_BANK_NAME'),
			'a.`bank_user_name`' => JText::_('COM_EXPORT_EXCEL_EXPORTEXCELS_BANK_USER_NAME'),
			'a.`bank_account_number`' => JText::_('COM_EXPORT_EXCEL_EXPORTEXCELS_BANK_ACCOUNT_NUMBER'),
		);
	}

    /**
     * Check if state is set
     *
     * @param   mixed  $state  State
     *
     * @return bool
     */
    public function getState($state)
    {
        return isset($this->state->{$state}) ? $this->state->{$state} : false;
    }
}
