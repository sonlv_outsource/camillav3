<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');
JHtml::_('formbehavior.chosen', 'select');
// print_r($this->item);

$user   = JFactory::getUser($this->item->id);
$groups = $user->get('groups');
$is_staff_group = 0;

foreach ($groups as $group)
{
    //echo '<p>Group = ' . $group . '</p>';
		if($group == 10){
				$is_staff_group = 1;
		}
}

JFactory::getDocument()->addScriptDeclaration("
	Joomla.submitbutton = function(task)
	{
		if (task == 'user.cancel' || document.formvalidator.isValid(document.getElementById('user-form')))
		{
			Joomla.submitform(task, document.getElementById('user-form'));
		}
	};

	Joomla.twoFactorMethodChange = function(e)
	{
		var selectedPane = 'com_users_twofactor_' + jQuery('#jform_twofactor_method').val();

		jQuery.each(jQuery('#com_users_twofactor_forms_container>div'), function(i, el) {
			if (el.id != selectedPane)
			{
				jQuery('#' + el.id).hide(0);
			}
			else
			{
				jQuery('#' + el.id).show(0);
			}
		});
	};
");

// Get the form fieldsets.
$fieldsets = $this->form->getFieldsets();
?>
<div id="information">
<div class="panel panel-default">
<div class="panel-heading">
	<h2 class="panel-title">Thông tin Đại lý</h2>
</div>
<div class="panel-body">
<form action="<?php echo JRoute::_('index.php?option=com_users&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="user-form" class="form-validate form-horizontal" enctype="multipart/form-data">

	<?php echo JLayoutHelper::render('joomla.edit.item_title', $this); ?>

	<fieldset>
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>

		<?php if ($this->grouplist && $_GET['id'] == 0) : ?>
			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'groups', JText::_('COM_USERS_ASSIGNED_GROUPS')); ?>
				<?php echo $this->loadTemplate('groups'); ?>
			<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php endif; ?>
			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_USERS_USER_ACCOUNT_DETAILS')); ?>
				<?php foreach ($this->form->getFieldset('user_details') as $field) : ?>
					<div class="control-group group-<?php echo $field->fieldname; ?> <?php if($is_staff_group == 0 && ($field->fieldname == 'stock_id' || $field->fieldname == 'is_stock_manager')){ ?>is_staff<?php } ?>"

						>

						<div class="control-label">
							<?php if($field->fieldname == 'id'):?>
								Mã TV<br><br>
							<?php endif; ?>
							<?php echo $field->label; ?>

						</div>
						<div class="controls" valign="top">
							<?php if($field->fieldname == 'id'):?>
								<?php if($this->item->id > 0){ ?>
								<?php echo $this->item->level.str_pad($this->item->id,6,"0",STR_PAD_LEFT); ?>
								<?php
								if($this->item->approved == 0){
									$approved = 'Chưa Duyệt';
									$btn =  'btn-warning';
								}
								if($this->item->approved == 1){
									$approved = 'Đã Cập nhật';
									$btn = 'btn-info';
								}
								if($this->item->approved == 9){
									$approved = 'Đã Duyệt';
									$btn = 'btn-success';
								}
								?>
								&nbsp;&nbsp;&nbsp;
								<span class="<?php echo $btn; ?>"><?php echo $approved; ?></span>
							<?php }else{ ?>
								#
							<?php } ?>
								<br><br>
							<?php endif; ?>
							<?php if ($field->fieldname == 'password') : ?>
								<?php // Disables autocomplete ?> <input type="password" style="display:none">
							<?php endif; ?>
							<?php echo $field->input; ?>
							<?php if ($field->fieldname == 'stock_id') : ?><button id="choose-stock" type="button" class="btn btn-warning" onclick="chooseStock()">Chọn Kho</button>
								<!-- &nbsp;&nbsp;<span id="showalert">Click chọn nhóm "Nhân Viên Kho" để có thể chọn Kho</span> -->
								<?php if($_GET['id'] == 0 ){ ?>
									<br><br><span id="namestock"></span>
							<?php } ?>

							<?php if($this->item->stock_id > 0){ ?> <br><br><?php  $stock = $this->getStockName($this->item->stock_id); echo $stock->title;  ?> - <?php echo $this->getProvinceName($stock->province); ?> <?php } ?>

							<?php endif; ?>

							<?php if($field->fieldname == 'invited_id'):?>
								<?php if($this->item->id > 0){ ?>
								<?php
								$userinfo = JFactory::getUser($this->item->invited_id);
								if($userinfo->id > 0){
								//echo 'Cấp '.$userinfo->level.':';
								}
								?>
								<a href="<?php echo JRoute::_('index.php?option=com_users&task=user.edit&id=' . (int) $this->item->invited_id); ?>" >
									<?php if($this->item->invited_id > 0){


									echo $userinfo->level.str_pad($userinfo->id,6,"0",STR_PAD_LEFT);

									}
									?>
								</a>
								<?php
								if($userinfo->id > 0){
									echo ' ( '.$userinfo->username.' - '.$userinfo->name.' ) ';
								}
								?>
							<?php }else{ ?>
								#
								<?php } ?>
							<?php endif; ?>

							<div class="parent-c1">
								<?php if($field->fieldname == 'invited_id'):?>
									<?php if($this->item->parent_c1 > 0){ ?>
									<?php
									$userinfo = JFactory::getUser($this->item->parent_c1);
									if($userinfo->id > 0){
									echo '<br>Cấp 1:';
									}
									?>
									<a href="<?php echo JRoute::_('index.php?option=com_users&task=user.edit&id=' . (int) $this->item->parent_c1); ?>" >
										<?php if($this->item->parent_c1 > 0){


										echo $userinfo->level.str_pad($userinfo->id,6,"0",STR_PAD_LEFT);

										}
										?>
									</a>
									<?php
									if($userinfo->id > 0){
										echo ' ( '.$userinfo->username.' - '.$userinfo->name.' ) ';
									}
									?>
								<?php }else{ ?>

									<?php } ?>
								<?php endif; ?>
							</div>
							<?php if ($field->fieldname == 'card_front' && $this->item->card_front) : ?>
                                <a href="<?php echo JURI::root(); ?>images/profile/<?php echo htmlspecialchars($this->item->card_front); ?>" target="_blank">
                                    <img src="<?php echo JURI::root(); ?>images/profile/<?php echo htmlspecialchars($this->item->card_front); ?>" alt="<?php echo htmlspecialchars($this->item->card_front); ?>" width="300" height="200" />
                                </a>
							<?php endif; ?>
							<?php if ($field->fieldname == 'card_behind' && $this->item->card_behind) : ?>
                            <a href="<?php echo JURI::root(); ?>images/profile/<?php echo htmlspecialchars($this->item->card_behind); ?>" target="_blank">
                                <img src="<?php echo JURI::root(); ?>images/profile/<?php echo htmlspecialchars($this->item->card_behind); ?>" alt="<?php echo htmlspecialchars($this->item->card_behind); ?>" width="300" height="200" />
                            </a>
							<?php endif; ?>
						</div>
					</div>
				<?php endforeach; ?>
			<?php echo JHtml::_('bootstrap.endTab'); ?>


			<?php if ($this->grouplist && $_GET['id'] > 0) : ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'groups', JText::_('COM_USERS_ASSIGNED_GROUPS')); ?>
					<?php echo $this->loadTemplate('groups'); ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
			<?php endif; ?>

			<?php
			$this->ignore_fieldsets = array('user_details');
			echo JLayoutHelper::render('joomla.edit.params', $this);
			?>

		<?php if (!empty($this->tfaform) && $this->item->id) : ?>
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'twofactorauth', JText::_('COM_USERS_USER_TWO_FACTOR_AUTH')); ?>
		<div class="control-group">
			<div class="control-label">
				<label id="jform_twofactor_method-lbl" for="jform_twofactor_method" class="hasTooltip"
						title="<?php echo '<strong>' . JText::_('COM_USERS_USER_FIELD_TWOFACTOR_LABEL') . '</strong><br />' . JText::_('COM_USERS_USER_FIELD_TWOFACTOR_DESC'); ?>">
					<?php echo JText::_('COM_USERS_USER_FIELD_TWOFACTOR_LABEL'); ?>
				</label>
			</div>
			<div class="controls">
				<?php echo JHtml::_('select.genericlist', Usershelper::getTwoFactorMethods(), 'jform[twofactor][method]', array('onchange' => 'Joomla.twoFactorMethodChange()'), 'value', 'text', $this->otpConfig->method, 'jform_twofactor_method', false); ?>
			</div>
		</div>
		<div id="com_users_twofactor_forms_container">
			<?php foreach ($this->tfaform as $form) : ?>
			<?php $style = $form['method'] == $this->otpConfig->method ? 'display: block' : 'display: none'; ?>
			<div id="com_users_twofactor_<?php echo $form['method'] ?>" style="<?php echo $style; ?>">
				<?php echo $form['form'] ?>
			</div>
			<?php endforeach; ?>
		</div>

		<fieldset>
			<legend>
				<?php echo JText::_('COM_USERS_USER_OTEPS'); ?>
			</legend>
			<div class="alert alert-info">
				<?php echo JText::_('COM_USERS_USER_OTEPS_DESC'); ?>
			</div>
			<?php if (empty($this->otpConfig->otep)) : ?>
			<div class="alert alert-warning">
				<?php echo JText::_('COM_USERS_USER_OTEPS_WAIT_DESC'); ?>
			</div>
			<?php else : ?>
			<?php foreach ($this->otpConfig->otep as $otep) : ?>
			<span class="span3">
				<?php echo substr($otep, 0, 4); ?>-<?php echo substr($otep, 4, 4); ?>-<?php echo substr($otep, 8, 4); ?>-<?php echo substr($otep, 12, 4); ?>
			</span>
			<?php endforeach; ?>
			<div class="clearfix"></div>
			<?php endif; ?>
		</fieldset>

		<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php endif; ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
	</fieldset>

	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
</div>
</div>
</div>
<div id="invited_id">
<?php
$month_selected = date('m');
$year_selected = date('Y');
?>
<div class="panel panel-default">
	<div class="text-red">Tiền hoa hồng được trả cho Đại lý vào ngày 10 hàng tháng.</div>
	<div class="text-black">
		 Hoa hồng được tính với đơn hàng đã được Kế toán duyệt <b>"Đã thanh toán"</b> từ <span class="text-red">00:00 ngày 10/<?php echo date("m") == 1?'12': date("m") - 1; ?>/<?php echo date("m") == 1?date('Y')-1: date("Y"); ?> <b>đến</b> 23:59 ngày 09/<?php echo date("m"); ?>/<?php echo date("Y"); ?></span>.
	</div>
	<br>
	<div class="panel-heading">
		<h2 class="panel-title">Danh số và Hoa hồng <?php echo date('m'),'/'.date('Y'); ?></h2>
	</div>
	<div class="panel-body">
		Cá nhân (CN):&nbsp;
		<span class="price"><?php
		$type = 'individual';

		$money3 = EshopHelper::getRevenueAmount($this->item->id,$month_selected,$year_selected,$type);
		echo number_format($money3,0,".",".");
		 ?>
	 </span>
		 <br>
		 Nhóm:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		 <span class="price">
		 <?php
		$type = 'group';

		$money4 = EshopHelper::getRevenueAmount($this->item->id,$month_selected,$year_selected,$type);
		echo number_format($money4,0,".",".");
		 ?>
	 	</span>
		 <br>
		 Hoa hồng:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		 <span class="price">
		<?php
		$type = 'group';
		$money5 = EshopHelper::getCommissionAmount($this->item->id,$month_selected,$year_selected,$type);
		echo number_format($money5,0,".",".");
		 ?>
		</span>
	</div>
	<div class="panel-heading">
		<h2 class="panel-title">Danh sách Bảo trợ</h2>
	</div>
	<div class="panel-body">

	<?php
		if($this->item->id > 0){
			$childs = $this->listChildUserByLevel($this->item->id,0);

		}

		//print_r($childs);
		if(count($childs) > 0){

				?>

				<div class="rTable">
				<div class="rTableRow">
				<div class="rTableHead"><span style="font-weight: bold;">Họ tên</span></div>
				<div class="rTableHead"><span style="font-weight: bold;">Mã TV</span></div>
				<div class="rTableHead"><span style="font-weight: bold;">Cấp ĐL</span></div>
				<div class="rTableHead"><span style="font-weight: bold;">Tỉnh/TP</span></div>
				<div class="rTableHead"><span style="font-weight: bold;">Doanh số & HH <?php echo date('m'),'/'.date('Y'); ?></span></div>
				<div class="rTableHead"><span style="font-weight: bold;">Ngày tạo</span></div>
				</div>
				<?php
				//print_r($childs); die;
				foreach($childs as $child){

					//print_r($child);
					?>
				<div class="rTableRow">
				<div class="rTableCell"><a href="<?php echo JRoute::_('index.php?option=com_users&task=user.edit&id=' . (int) $child->id); ?>" ><?php echo $child->name; ?></a></div>
				<div class="rTableCell">
					<a href="<?php echo JRoute::_('index.php?option=com_users&task=user.edit&id=' . (int) $child->id); ?>" ><?php echo $child->level.str_pad($child->id,6,"0",STR_PAD_LEFT); ?></a>
					<div class"user-status">
						<?php
						if($child->approved == 0){
							$approved = 'Chưa Duyệt';
							$btn =  'btn-warning';
						}
						if($child->approved == 1){
							$approved = 'Đã Cập nhật';
							$btn = 'btn-info';
						}
						if($child->approved == 9){
							$approved = 'Đã Duyệt';
							$btn = 'btn-success';
						}
						?>
						<span class="<?php echo $btn; ?>"><?php echo $approved; ?></span>
					</div>
				</div>
				<div class="rTableCell">C<?php echo $child->level; ?></div>
				<div class="rTableCell"><?php echo $this->getProvinceName($child->province); ?></div>
				<div class="rTableCell">

					Cá nhân:
					<span class="price"><?php
					$type = 'individual';

					$money1 = EshopHelper::getRevenueAmount($child->id,$month_selected,$year_selected,$type);
					echo number_format($money1,0,".",".");
					 ?>
				 </span>
					 <br>
					 Nhóm:
					 <span class="price">
					 <?php
 					$type = 'group';

 					$money2 = EshopHelper::getRevenueAmount($child->id,$month_selected,$year_selected,$type);
 					echo number_format($money2,0,".",".");
 					 ?>
					</span>
					<br>
					HH:
					<span class="price">
					<?php
					$type = 'group';
					$money6 = EshopHelper::getCommissionAmount($child->id,$month_selected,$year_selected,$type);
					echo number_format($money6,0,".",".");
					 ?>
					</span>
				</div>
				<div class="rTableCell"><?php echo date("d-m-Y",strtotime($child->registerDate)); ?></div>
				</div>
				<?php } ?>

				</div>
				<?php


		}else{
			echo "Không có Đại lý!";
		}
	?>
</div>
</div>
</div>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel">GÁN KHO</h3>
            </div>
            <div class="modal-body">
                <div class="form-control-lg">
                    <div class="form-group row-fluid">
                        <label for="shipper" class="form-control-label span2">&nbsp;&nbsp;<strong>Chọn Kho:</strong></label>
		                <?php
		                $list = $this->getListStock();
		                ?>
                       <div class="span10">
                           <select name="shipper" id="shipper">
                               <option value="0">Vui lòng chọn</option>
		                       <?php foreach ($list as $item){?>
                                   <option value="<?php echo $item->id;?>"><?php echo $item->title;?> - <?php echo $this->getProvinceName($item->province); ?></option>
		                       <?php }?>
                           </select>
                           <div class="has-error error-shipper" style="color:red;"></div>
                       </div>
											 <input type="hidden" id="idrequest" name="idrequest" value="<?php echo $this->item->id; ?>">
											 <input type="hidden" id="is_stock_manager" name="is_stock_manager" value="<?php echo $this->item->is_stock_manager; ?>">

                    </div>

                </div>
                <div class="has-error error-msg"></div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="saveChange()" id="saveChange" class="btn btn-success">Lưu lại</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<script>
js = jQuery.noConflict();

js(document).ready(function () {
	<?php if($_GET['id'] > 0 ){ ?>
	if (js('#1group_10').is(":checked"))
	{
		js('#choose-stock').attr("disabled",false);
		js('#jform_is_stock_manager0').attr('disabled',false);
		js('#jform_is_stock_manager1').attr('disabled',false);


		js('#showalert').css("display","none");

	}else{
		//js('#jform_is_stock_manager input').attr('disabled','disabled');
		//js("#jform_is_stock_manager0").attr('disabled',true);
		js('#choose-stock').attr("disabled",true);
		js('#jform_is_stock_manager0').attr('disabled',true);
		js('#jform_is_stock_manager1').attr('disabled',true);
		js('#showalert').css("display","block");
		js('.group-level').css("display","none");

	}
	<?php }else{ ?>
		js('#1group_2').prop('checked', false);
		js('#1group_10').prop('checked', true);
		js('#1group_8').prop('checked', false);

		//js('#jform_stock_id').val(100);
	<?php } ?>



	js('#jform_level').change(function() {
			if(js(this).val() > 1){
				alert('Admin không được tạo khác Cấp 1.');
				js('#jform_level').val(1).trigger("liszt:updated");
			}
  });

	js('input[name="jform[groups][]"]').click(function(){
		js('#1group_2').prop('checked', false);
		js('#1group_6').prop('checked', false);
		js('#1group_10').prop('checked', false);
		js('#1group_'+js(this).val()).prop('checked', true);
		if(js(this).val() == 10 && js(this).is(":checked")){
				js('#choose-stock').attr("disabled",false);
				js('#showalert').css("display","block");
				js('#jform_is_stock_manager0').attr('disabled',false);
				js('#jform_is_stock_manager1').attr('disabled',false);
				js('#showalert').css("display","none");
				js('.group-level').css("display","block");
		}else{
				js('#choose-stock').attr("disabled",true);
				js('#showalert').css("display","none");
				js('#jform_is_stock_manager0').attr('disabled',true);
				js('#jform_is_stock_manager1').attr('disabled',true);
				js('#showalert').css("display","block");
				js('.group-level').css("display","none");
		}
	});
});


var myModal = js('#myModal');
function chooseStock(){
	myModal.modal('show');
}

function saveChange () {

		var valid = true;
		var idShipper = jQuery('#shipper').val();
		var idRequest = jQuery('#idrequest').val();
		var is_stock_manager = jQuery('#is_stock_manager').val();
		if (idShipper == '0'){
				js('.error-shipper').html('Bạn vui lòng chọn Kho!');
				valid = false;
		}else {
				js('.error-shipper').html('');
				valid = true;
		}
		<?php if($_GET['id'] == 0 ){ ?>
			jQuery('#jform_stock_id').val(jQuery('#shipper').val());
			jQuery('#namestock').html(jQuery( "#shipper option:selected" ).text());
			myModal.modal('hide');
		<?php } ?>

		<?php if($_GET['id'] > 0 ){ ?>
		if (valid == true){
			js('.error-shipper').html('');
			js.ajax({
					url: "<?php echo JURI::root(); ?>administrator/index.php?option=com_users&task=user.assignStock&tmpl=component",
					method: 'POST',
					data:{idShipper: idShipper, idRequest: idRequest,is_stock_manager:is_stock_manager} ,
					success: function (result) {
							if(result == '1') {
									alert('Gán Kho thành công!');

									window.setTimeout('location.reload()', 300);

							}else{
									js('.error-msg').html('Có lỗi xảy ra, vui lòng thử lại!');
							}
					}
			});
		}
		<?php } ?>

	}
</script>

<style>
.price{ color:red;}
#invited_id{ width: 50%; float:left; }
#information{ width: 50%; float:left; }
.panel-title{ color:#12489c;}
#jform_invited_id{
display:none;
}
#jform_card_front, #jform_card_behind{
    display: none;
}
#myModal{width:50%;}
#myModal .chzn-container{width:80%!important;}
label.checkbox[for="1group_1"]
{
  display:none!important;
}
label.checkbox[for="1group_3"]
{
  display:none!important;
}
label.checkbox[for="1group_4"]
{
  display:none!important;
}
label.checkbox[for="1group_9"]
{
  display:none!important;
}
label.checkbox[for="1group_8"]
{
  display:none!important;
}
label.checkbox[for="1group_5"]
{
  display:none!important;
}
label.checkbox[for="1group_7"]
{
  display:none!important;
}
#groups.tab-pane .control-group{margin-bottom: 0px!important;}



.rTable {
  	display: table;
  	width: 100%;
}
.rTableRow {
  	display: table-row;
}
.rTableHeading {
  	display: table-header-group;
  	background-color: #ddd;
}
.rTableCell, .rTableHead {
  	display: table-cell;
  	padding: 10px 10px;
  	border: 1px solid #999999;
}
.rTableHeading {
  	display: table-header-group;
  	background-color: #ddd;
  	font-weight: bold;
}
.rTableFoot {
  	display: table-footer-group;
  	font-weight: bold;
  	background-color: #ddd;
}
.rTableBody {
  	display: table-row-group;
}
.text-orange{
	color:orange;
	font-size: 15px;
}
.text-red{
	color:red;
	font-size: 15px;
}
.text-price-red{
	color:red;
}
.text-black{
	color:black;
	font-size: 15px;
}
</style>
