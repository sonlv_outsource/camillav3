<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$doc       = JFactory::getDocument();
$direction = $doc->direction == 'rtl' ? 'pull-right' : '';
$class     = $enabled ? 'nav ' . $direction : 'nav disabled ' . $direction;

// Recurse through children of root node if they exist
$menuTree = $menu->getTree();
$root     = $menuTree->reset();

if ($root->hasChildren())
{
	echo '<ul id="menu" class="' . $class . '">' . "\n";

	// WARNING: Do not use direct 'include' or 'require' as it is important to isolate the scope for each call
	$menu->renderSubmenu(JModuleHelper::getLayoutPath('mod_menu', 'default_submenu'));
	//nganly

	$user   = JFactory::getUser();

	$groups = $user->get('groups');

	$hide_delete = 0;
	foreach ($groups as $group)
	{
	    if($group == 6){
	      $hide_delete = 1;
	    }
	}

	$hide_group_class = '';
	if($hide_delete == 1){
		$hide_group_class = 'class="hidde-group"';
	}


	echo '<li><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=orders">Đơn hàng</a></li>';
	echo '<li><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_users&view=sales">Xuất Excel HH Đại lý</a></li>';
	echo '<li ><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=stockreports">Thống kê Đơn hàng</a></li>';
	echo '<li ><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_users&view=users">Đại lý / KH</a></li>';
	echo '<li '.$hide_group_class.'><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_notification&view=notifications">Thông báo trên App</a></li>';

	echo '<li '.$hide_group_class.'><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=products">Sản phẩm</a></li>';
	echo '<li '.$hide_group_class.'><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=products&template=updateprice">Quản lý Giá</a></li>';
	echo '<li '.$hide_group_class.'><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_config&view=component&component=com_configs">Hoa hồng C1-C5</a></li>';

  echo '<li '.$hide_group_class.'><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=categories">Danh mục Sản phẩm</a></li>';
	echo '<li '.$hide_group_class.'><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=manufacturers">Nhà sản xuất</a></li>';
	echo '<li '.$hide_group_class.'><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=options">Tuỳ chọn (Option)</a></li>';
	echo '<li '.$hide_group_class.'><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=weights">Đơn vị</a></li>';

	// echo '<li ><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=productreports">Xuất Excel ĐH</a></li>';


	echo '<li '.$hide_group_class.'><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_banners">Quản lý Banner</a></li>';
	// echo '<li><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_store">'.JText::_('MOD_MENU_PRODUCT_STORE').'</a></li>';
	//echo '<li><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_book">'.JText::_('MOD_MENU_PRODUCT_BOOKING').'</a></li>';


	// echo '<li '.$hide_group_class.'><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=coupons">Coupon</a></li>';
	// echo '<li '.$hide_group_class.'><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_eshop&view=vouchers">Voucher</a></li>';
	echo '<li '.$hide_group_class.'><a class="dropdown-toggle" data-toggle="dropdown" href="index.php?option=com_content">Tin tức & Bài viết</a></li>';

	echo "</ul>\n";

	echo '<ul id="nav-empty" class="dropdown-menu nav-empty hidden-phone"></ul>';

	if ($css = $menuTree->getCss())
	{
		$doc->addStyleDeclaration(implode("\n", $css));
	}
}
