<?php
/**
 * @package         SCLogin
 * @copyright (c)   2009-2018 by SourceCoast - All Rights Reserved
 * @license         http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @version         Release v8.0.3
 * @build-date      2018/08/20
 */

defined('_JEXEC') or die('Restricted access');

?>
<div class="sclogin sourcecoast">

<?php
if ($params->get('enableProfilePic'))
    echo $helper->getSocialAvatar($registerType, $helper->profileLink);

if ($params->get('greetingName') != 2)
{
    $user = JFactory::getUser();
    if ($params->get('greetingName') == 0)
        $name = $user->get('username');
    else
        $name = $user->get('name');
    echo '<div class="sclogin-greeting">' . JText::sprintf('MOD_SCLOGIN_WELCOME', $name) . '</div>';
}
?>
	<?php
	if ($params->get('showUserMenu'))
	{
		echo $helper->getUserMenu($params->get('showUserMenu'), $params->get('userMenuStyle'), $params->get('userMenuTitle'), $name);
	}

	if ($params->get('showConnectButton'))
	{ ?>
    <div class="sclogin-social-connect">
		<?php echo $helper->getReconnectButtons($params->get('socialButtonsOrientation'), $params->get('socialButtonsAlignment'));?>
    </div>
	<?php
	}

	echo $helper->getPoweredByLink();
	?>
<?php
if ($params->get('showLogoutButton'))
{
    if($params->get('showLogoutButton') == 1)
        $logoutClass='button btn btn-primary';
    else
        $logoutClass='logout-link';
    ?>
    <div class="sclogout-button">
        <div class="sclogin-joomla-login">
            <form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure'));?>" method="post" id="sclogin-form">
                <div class="logout-button" id="scLogoutButton">
                    <button type="submit" name="Submit" title="Thoát" class="<?php echo $logoutClass;?>" ><i class="fa fa-sign-out" aria-hidden="true"></i> <span class="txt txt_logout"><?php echo JText::_('JLOGOUT');?></span></button>

                    <?php $option = JFactory::getApplication()->input->get('option');?>
                    <?php if($option == 'com_easysocial'):?>
                    <input type="hidden" name="option" value="com_easysocial" />
                    <input type="hidden" name="controller" value="account" />
                    <input type="hidden" name="task" value="logout" />
                    <?php else:?>
                    <input type="hidden" name="option" value="com_users" />
                    <input type="hidden" name="task" value="user.logout" />
                    <?php endif;?>

                    <input type="hidden" name="return" value="<?php echo $jLogoutUrl;?>" />
                    <?php echo JHtml::_('form.token')?>
                </div>
            </form>
        </div>
    </div>
<?php
}
?>

    <div class="clearfix"></div>
</div>
