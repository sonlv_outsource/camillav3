
ALTER TABLE `prfwj_eshop_products` CHANGE `product_weight` `product_weight` DECIMAL(15,2) NULL DEFAULT NULL;
ALTER TABLE `prfwj_eshop_productoptionvalues` CHANGE `price` `price` DECIMAL(15,0) NULL DEFAULT NULL;
ALTER TABLE `prfwj_eshop_productoptionvalues` CHANGE `weight` `weight` DECIMAL(15,2) NULL DEFAULT NULL;

----- Daily

ALTER TABLE `prfwj_users` ADD `sex` VARCHAR(1) NOT NULL AFTER `name`;
ALTER TABLE `prfwj_users` CHANGE `sex` `sex` VARCHAR(1) NOT NULL;
ALTER TABLE `prfwj_users` ADD `card_id` VARCHAR(15) NOT NULL AFTER `sex`;
ALTER TABLE `prfwj_users` ADD `birthday` DATETIME NOT NULL default '1000-01-01 00:00:00' AFTER `card_id`;
ALTER TABLE `prfwj_users` ADD `province` VARCHAR(3) NOT NULL AFTER `birthday`;
ALTER TABLE `prfwj_users` ADD `level` INT NOT NULL AFTER `province`;
ALTER TABLE `prfwj_users` ADD `job` VARCHAR(2) NOT NULL AFTER `level`;
ALTER TABLE `prfwj_users` ADD `card_front` VARCHAR(255) NOT NULL AFTER `job`, ADD `card_behind` VARCHAR(255) NOT NULL AFTER `card_front`;
ALTER TABLE `prfwj_users` ADD `invited_id` INT NOT NULL AFTER `id`;

---- Chinh sua dai ly

ALTER TABLE `prfwj_users` ADD `approved` TINYINT(4) NOT NULL DEFAULT '0' AFTER `block`;
ALTER TABLE `prfwj_users` CHANGE `approved` `approved` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0: chua approve, 1: da cap nhat thong tin, 9: duoc phe duyet';
ALTER TABLE `prfwj_eshop_orders` CHANGE `payment_status` `payment_status` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0: dang cho, 1: da cap nhat thong tin thanh toan, 9: Da duyet';
ALTER TABLE `prfwj_eshop_orders` ADD `payment_image` VARCHAR(255) NOT NULL AFTER `payment_status`;


-- copy tuoikia
ALTER TABLE `prfwj_eshop_products` ADD `stock_modified_date` DATETIME NULL DEFAULT NULL AFTER `checked_out_time`, ADD `price_modified_date` DATETIME NULL DEFAULT NULL AFTER `stock_modified_date`;


ALTER TABLE `prfwj_eshop_products` ADD `product_total_weight` FLOAT NOT NULL DEFAULT '0' AFTER `price_modified_date`;
ALTER TABLE `prfwj_eshop_products` ADD `product_price_discounted` FLOAT NOT NULL DEFAULT '0' AFTER `product_total_weight`;
ALTER TABLE `prfwj_eshop_products` ADD `product_discount` FLOAT NOT NULL DEFAULT '0' AFTER `product_price_discounted`;

ALTER TABLE `prfwj_eshop_products` ADD `product_discount_from_date` DATETIME NULL AFTER `product_discount`, ADD `product_discount_to_date` DATETIME NULL AFTER `product_discount_from_date`;


DELETE FROM `prfwj_eshop_optionvaluedetails` WHERE `prfwj_eshop_optionvaluedetails`.`option_id` NOT IN(4);
DELETE FROM `prfwj_eshop_optionvalues` WHERE `prfwj_eshop_optionvalues`.`option_id` NOT IN(4);


DROP TABLE IF EXISTS `prfwj_social`;

CREATE TABLE `prfwj_social` (
  `social_type` VARCHAR(32) NOT NULL,
  `field_value` VARCHAR(255) NOT NULL,
  `token` VARCHAR(64) NOT NULL,
  `verify_code` VARCHAR(16) NOT NULL,
  `created_date` DATETIME NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `prfwj_user_social`;

CREATE TABLE `prfwj_user_social` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `social_id` VARCHAR(32) CHARACTER SET utf8mb4 NOT NULL,
  `social_name` VARCHAR(16) CHARACTER SET utf8mb4 NOT NULL,
  `name` VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `email` VARCHAR(255) DEFAULT NULL,
  `phone` VARCHAR(255) DEFAULT NULL,
  `user_id` INT(11) NOT NULL,
  `created_date` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

TRUNCATE prfwj_eshop_order_history;

ALTER TABLE `prfwj_eshop_orders` ADD `cms_done_date` DATETIME NULL DEFAULT NULL COMMENT 'date to count commission' AFTER `ship_distance`, ADD `cms_status` TINYINT NOT NULL DEFAULT '0' COMMENT 'commision status 0:waiting, 1: done' AFTER `cms_done_date`;
ALTER TABLE `prfwj_eshop_orders` ADD `cms_c1` INT NOT NULL AFTER `cms_status`, ADD `cms_c2` INT NOT NULL AFTER `cms_c1`, ADD `cms_c3` INT NOT NULL AFTER `cms_c2`, ADD `cms_c4` INT NOT NULL AFTER `cms_c3`, ADD `cms_c5` INT NOT NULL AFTER `cms_c4`, ADD `cms_active` INT NOT NULL AFTER `cms_c5`;
ALTER TABLE `prfwj_eshop_orders` ADD `cms_discount_c1` FLOAT NOT NULL AFTER `cms_active`, ADD `cms_discount_c2` FLOAT NOT NULL AFTER `cms_discount_c1`, ADD `cms_discount_c3` FLOAT NOT NULL AFTER `cms_discount_c2`, ADD `cms_discount_c4` FLOAT NOT NULL AFTER `cms_discount_c3`, ADD `cms_discount_c5` FLOAT NOT NULL AFTER `cms_discount_c4`;
ALTER TABLE `prfwj_eshop_orders` ADD `cms_money_c1` INT NOT NULL AFTER `cms_discount_c5`, ADD `cms_money_c2` INT NOT NULL AFTER `cms_money_c1`, ADD `cms_money_c3` INT NOT NULL AFTER `cms_money_c2`, ADD `cms_money_c4` INT NOT NULL AFTER `cms_money_c3`, ADD `cms_money_c5` INT NOT NULL AFTER `cms_money_c4`;


ALTER TABLE `prfwj_users` ADD `bank_name` VARCHAR(255) NOT NULL AFTER `requireReset`,
ADD `bank_account_name` VARCHAR(255) NOT NULL AFTER `bank_name`,
ADD `bank_account_number` VARCHAR(255) NOT NULL AFTER `bank_account_name`;

ALTER TABLE `prfwj_users` ADD `parent_c1` INT NOT NULL AFTER `bank_account_number`;

ALTER TABLE `prfwj_eshop_weights` CHANGE `exchanged_value` `exchanged_value` DECIMAL( 15, 1 ) NULL DEFAULT NULL
