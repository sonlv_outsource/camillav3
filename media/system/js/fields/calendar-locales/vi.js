window.JoomlaCalLocale = {
	today : "Hôm nay",
	weekend : [0, 6],
	wk : "wk",
	time : "Time:",
	days : ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
	shortDays : ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
	months : ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "Tháng 12"],
	shortMonths : ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
	AM : "AM",
	PM :  "PM",
	am : "am",
	pm : "pm",
	dateType : "gregorian",
	minYear : 1900,
	maxYear : 2100,
	exit: "Close",
	save: "Clear"
};
